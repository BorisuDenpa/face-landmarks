import cv2
import numpy as np
import math
import keyboard

# Loads the predefined landmarks
landmarks = np.loadtxt("landmarks.txt", dtype='i', delimiter=',')
# Reads and stores a copy of the original image
img = cv2.imread('cara1.jpg')
img2 = img.copy()
h, w = img.shape[:2]

# Draws initial landmarks on screen
for i in range(0, 32):
    cv2.circle(img, (landmarks[i][0],landmarks[i][1]), 3, (255, 0,0), -1)


def nothing(x):
    pass
# Draws lines between the landmark points
def refreshface():
    for i in range(0, 1):
        cv2.line(img, (landmarks[i][0],landmarks[i][1]), (landmarks[i+1][0],landmarks[i+1][1]), (0, 255, 0), 1)
    for i in range(2, 3):
        cv2.line(img, (landmarks[i][0],landmarks[i][1]), (landmarks[i+1][0],landmarks[i+1][1]), (0, 255, 0), 1)
    for i in range(4, 7):
        cv2.line(img, (landmarks[i][0],landmarks[i][1]), (landmarks[i+1][0],landmarks[i+1][1]), (0, 255, 0), 1)
    cv2.line(img, (landmarks[7][0],landmarks[7][1]), (landmarks[4][0],landmarks[4][1]), (0, 255, 0), 1)
    for i in range(8, 11):
        cv2.line(img, (landmarks[i][0],landmarks[i][1]), (landmarks[i+1][0],landmarks[i+1][1]), (0, 255, 0), 1)
    cv2.line(img, (landmarks[11][0],landmarks[11][1]), (landmarks[8][0],landmarks[8][1]), (0, 255, 0), 1)
    for i in range(12, 18):
        cv2.line(img, (landmarks[i][0],landmarks[i][1]), (landmarks[i+1][0],landmarks[i+1][1]), (0, 255, 0), 1)
    for i in range(19, 22):
        cv2.line(img, (landmarks[i][0],landmarks[i][1]), (landmarks[i+1][0],landmarks[i+1][1]), (0, 255, 0), 1)
    cv2.line(img, (landmarks[22][0],landmarks[22][1]), (landmarks[19][0],landmarks[19][1]), (0, 255, 0), 1)
    for i in range(23, 31):
        cv2.line(img, (landmarks[i][0],landmarks[i][1]), (landmarks[i+1][0],landmarks[i+1][1]), (0, 255, 0), 1)

# Show window with things
refreshface()

# Loads a window with the image
cv2.imshow('img', img)
cv2.namedWindow('img')

# Flag for active clicking and minimal range to consider as a close click to a landmark
click = 0
click2 = 2
mini = 8
# Guarda el
center = [int(h), int(w)]

# Mouse Events
def draw_circle(event,x,y,param,flags):

    global click, click2, img, mini, li, center
    if event == cv2.EVENT_LBUTTONDOWN:

        # Looks if the click is close enough to the point
        for i in range (0,32):
            xi = x - landmarks[i][0]
            yi = y - landmarks[i][1]
            d = math.sqrt((xi**2)+(yi**2))
            if d < mini:
                mini = d
                li = i
                mini = 8
                click = 1
            else:
                click2 = 1

    # When the click stops
    if event == cv2.EVENT_LBUTTONUP:
        click = 0
        click2 = 0
        print(x, y)

    # When the mouse is moving and the left button is down
    if event == cv2.EVENT_MOUSEMOVE:

        # If the click isn't over a point
        if click2 == 1:
            if keyboard.is_pressed('ctrl'):

                # Refreshes all the points according to the mouse position
                for i in range(0, 32):
                    if x>0 and y>0:
                        landmarks[i][0] = (center[0] - landmarks[i][0]) + x
                        landmarks[i][1] = (center[1] - landmarks[i][1]) + y
                        cv2.circle(img, (landmarks[i][0], landmarks[i][1]), 3, (255, 0, 0), -1)

            refreshface()
            cv2.imshow('img', img)
            img = img2.copy()

        if click == 1:

            # Saves the position of the landmark that is moving
            landmarks[li][0] = x
            landmarks[li][1] = y
            for i in range(0, 32):
                cv2.circle(img, (landmarks[i][0], landmarks[i][1]), 3, (255, 0, 0), -1)

            refreshface()
            # Updates the screen
            cv2.imshow('img', img)
            # Gets a copy of the original image
            img = img2.copy()

# Assigns a name to the window
cv2.namedWindow('img')

# Activates the mouse callback
cv2.setMouseCallback('img',draw_circle)
cv2.createTrackbar('R','img',0,255,nothing)


while(1):
    # Exits with ESC
    if cv2.waitKey(20) & 0xFF == 27:
        # Saves the modified landmarks
        np.savetxt('out.txt', landmarks, fmt='%10.0f', delimiter=',')
        break
cv2.destroyAllWindows()