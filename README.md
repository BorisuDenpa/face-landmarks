Requires   
Python 2.7   
Numpy 1.18   
OpenCV 3   

![Image 1](example.png)

Shows a pattern on top of pictures and allows you to modify the positions of the  
points using the mouse, intended for generating facelandmark samples to be use  
in thermal face recognition things. 